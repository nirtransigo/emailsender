#Author: Jason Robinson
#Orignally created: 06/08/2018

import common_stuff
import os
import logging
import datetime

RELATIVE_PATH_TO_LOGS      = "/Logs"
ABSOLUTE_PATH_TO_LOGS      = common_stuff.ABSOLUTE_PATH_TO_PROJECT_ORIGIN + RELATIVE_PATH_TO_LOGS

LOG_FILE_NAME = "EmailSenderLog"

'''
This function sets the debug logging configurations (where to write to, what format to use, etc).
It must be called once (and only once), at the start of the program. 

param  severity - none

return - none
'''
def InitLog() :

    logging.basicConfig(level=logging.INFO , filename="%s/%s" % (ABSOLUTE_PATH_TO_LOGS, LOG_FILE_NAME) , format='%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s')


'''
This function writes a formatted message to the debug log file, which includes the timestamp, the process details,
severity and the message itself.
The unformatted message is also printed to the console for easy "live" debugging. 

Note: Assumes the log file exist!

param  message  - string to report to log 
param  severity - string for one of the *standard* severity levels

return - none
'''

def WriteToLog (message , severity) :

    try:
        if (severity == "debug") :

            logging.debug (message)

        elif (severity == "info") :

            logging.info (message)
        elif (severity == "warning") :

            logging.warning (message)

        elif (severity == "error") :

            logging.error (message)

        elif (severity == "critical") :

            logging.critical (message)

        else:

            logging.debug (message) #default severity

    except Exception as ex :

        print ("Error writing to log!")

        print (ex)


    print (message) #for easier debugging when running the program manually


#######################################################################################################################################################################################3



