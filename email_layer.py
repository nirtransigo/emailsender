
#Author: Jason Robinson
#Orignally created: 31/07/2018

import common_stuff
from enum import Enum
import sendgrid
import re
import os
import glob
from sendgrid.helpers.mail import *
import log_layer
import rest_api_layer

FILLER_DELIMITER_TEXT = '-@-' #TODO: change and extend the delimiter, so we'll never match it by accident!

class SendType (Enum) :
    PLAIN = 1
    HTML  = 2

TEMPLATE_FILE_NAME_SUFFIX = ".txt"


#TODO: add return value and test it!
'''
This function uses Sendgrid's API to send an email with requested details.

Note1: the word "Raw" indicates that we *don't* rely on Sendgrid's template mechanism.
       instead, we store our templates locally, perform substitutions ourself (in the following function) and send to this function the exact content (including html symbols),
       which we "dump" on Sendgrid.
Note2: currently we only use the HTMP send_type.

param  origin_address      - address from which the email will be sent. Note: must be registered in Sendgrid under our account. 
param  destination_address - address to which the email will be sent.
param  subject             - desired subject of the email. should be a short string.
param  content             - the exact content to be sent, including both text and all html symbols for the design (header, sizes, images, etc).
param  send_type           - enum for the way we use Sendgrid's API. options are PLAIN or HTML (the latter allows us to format the email, add images, set sizes, etc).

return - none  
'''

def SendRawEmail (origin_address , destination_address ,subject , content , send_type) :

    sg = sendgrid.SendGridAPIClient(apikey=rest_api_layer.API_KEY)

    from_email = Email(origin_address)

    to_email = Email(destination_address)

    if (send_type == SendType.PLAIN) :

        formatted_content = Content("text/plain" , content)

    elif (send_type == SendType.HTML) :

        formatted_content = Content("text/html" , content)

    else:
        log_layer.WriteToLog ("unknown send_type!" , "error")

        raise ValueError('Illegal content type (not text/plain or text/html')

    mail = Mail (from_email , subject , to_email , formatted_content)

    response = sg.client.mail.send.post (request_body = mail.get ()) #actual communication with Sendgrid (all other actions in this module are local)

    log_layer.WriteToLog (response.status_code , "info")
    log_layer.WriteToLog (response.body , "info")
    log_layer.WriteToLog (response.headers , "info")


'''
This function performs substitutios of filler symbols (including their delimiters) inside of an email template, with desired text.

Note: this function *doesn't* perform any validation! the caller must validate the legality of the substitutions beforehand!

param  html_template_text -  raw template, including filler symbols.
param  substitutions      -  dictionary whose keys are (all) the fillers of the template, and values are the text we wish to replace them with. 

return - final email content (i.e the same template after performing all substitutions.  
'''

def SubstituteInTemplate (html_template_text , substitutions) :

    processed_template = html_template_text

    for key in substitutions :

        key_with_delimiters = FILLER_DELIMITER_TEXT + key + FILLER_DELIMITER_TEXT #so we won't have lingering delimiters in the final text

        processed_template = processed_template.replace (key_with_delimiters , substitutions[key])

    return processed_template


'''
This function generates an email with the requested details (subject, addresses, content, etc) 
and sends it using Sendgrid API.
It also validates that the substitutions are legal (i.e include all the fillers of the template and only them); if not, It refuses to send the email and raises an exception. 

Note: the email content must be derived from existing template, by performing all required substitutions in it.
      whenver a new "type" of email is desired, the template should be generated and saved in the appropriate folder, according to our template standards (see external documentation).

param  origin_address      - address from which the email will be sent. Note: must be registered in Sendgrid under our account. 
param  destination_address - address to which the email will be sent.
param  subject             - desired subject of the email. should be a short string.
param  template_name       - the name of the template we wish to base the email on. Note: this is the template name, not the file name (i.e no extension should appear).
param  substitutions       - dictionary whose keys are (all) the fillers of the template, and values are the text we wish to replace them with.

return - none
'''

def FormatAndSendEmail (origin_address , destination_address , subject , template_name , substitutions) :

    template_file_name = GetTemplateFileName(template_name)

    #open, read and close the file
    with open (template_file_name , 'r') as myfile :
        internal_template = myfile.read ()

    if (not AreSubstitutionsValid (internal_template , substitutions)) :
        log_layer.WriteToLog ("illegal substitutions" , "error")

        raise ValueError ('Illegal substitutions')

    processed_internal_template = SubstituteInTemplate (internal_template , substitutions)

    log_layer.WriteToLog ("sending email with following details:" , "info")
    log_layer.WriteToLog ("origin_address: %s" % (origin_address) , "info")
    log_layer.WriteToLog ("destination_address: %s" % (destination_address) , "info")
    log_layer.WriteToLog ("subject: %s" % (subject) , "info")

    SendRawEmail(origin_address , destination_address , subject, processed_internal_template, SendType.HTML)

def AreSubstitutionsValid (email_template , substitutions) :

    fillers_regex =  r'%s(.*?)%s' % (FILLER_DELIMITER_TEXT , FILLER_DELIMITER_TEXT)

    log_layer.WriteToLog (fillers_regex , "info") #testing!

    fillers_list = re.findall (fillers_regex , email_template)

    log_layer.WriteToLog (fillers_list , "info") #testing!

    for sub in substitutions :

        if (sub not in fillers_list) :

            log_layer.WriteToLog ("asked to substituted %s, but it's not a filler in this template!" % (sub) , "warning")

            return (False)

    for filler in fillers_list :

        if (filler not in substitutions) :

            log_layer.WriteToLog ("missing substitution for filler %s, required in this template!" % (filler) , "warning")

            return (False)

        log_layer.WriteToLog ("valid substitutions!" , "info") #testing

    return (True)

'''
This function returns all the fillers (i.e strings that should be substituted) in a template with a given name.
It does so by reading the template file and parsing for the filler format.

Note: the list may include duplicates (i.e if the same filler appears twice it the template, it'll also appear twice in the list).

param template_name - name without extensions 

return - list of all required fillers  
'''

def GetFillersList (template_name) :

    template_file_name = GetTemplateFileName (template_name)

    #open, read and close the file
    with open(template_file_name , 'r') as myfile :
        internal_template = myfile.read()

    #idetify all fillers (and only them)
    fillers_regex =  r'%s(.*?)%s' % (FILLER_DELIMITER_TEXT , FILLER_DELIMITER_TEXT)
    log_layer.WriteToLog (fillers_regex , "info") #testing!
    fillers_list = re.findall (fillers_regex , internal_template)

    log_layer.WriteToLog (fillers_list , "info") #testing!

    return (fillers_list)


'''
This function converts a template name to its file name, by adding the (internally )required suffix

param template_name - name without extensions 

return - template file name 
'''

def GetTemplateFileName (template_name) :

    return (template_name + TEMPLATE_FILE_NAME_SUFFIX)

'''
This function acquires and returns the complete list of support email templates.
The list is acquired by listing on the files in the appropriate directory and excluding all files which don't fit the format
(i.e contain the sub-string "template" and end with ".txt".

Note: the template name is the file name, *without the extension* (e.g "good_template.txt" will become "good_template"). 

param - none

return - list of template names 
'''

def GetTemplatesList () :

    REQUIRED_SUB_STRING = "template"

    raw_files_list = os.listdir()

    # validate substring
    files_list = [x for x in raw_files_list if REQUIRED_SUB_STRING in x]

    #validate ending
    ending_regex = re.compile ('.*\\' + TEMPLATE_FILE_NAME_SUFFIX + '$')      #note: the '\' is needed only because the required suffix starts with '.'
    files_list = [x for x in files_list if ending_regex.match(x)]

    #truncate ending
    suffix_len = len (TEMPLATE_FILE_NAME_SUFFIX)

    for i in range (len (files_list)):

        files_list[i] = (files_list[i])[:-suffix_len]


    return (files_list)

#######################################################################################################################################################################################3