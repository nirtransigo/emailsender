
#Author: Jason Robinson
#Orignally created: 31/07/2018


#TODO: disable debug mode before uploading code to production!

import common_stuff

from flask import Flask
from flask import request
from flask_restful import reqparse, abort, Api, Resource

import sendgrid
import os
import ast
from sendgrid.helpers.mail import *

import email_layer
import logging.config
import log_layer

RELATIVE_PATH_TO_TEMPLATES      = "/Templates"
RELATIVE_PATH_TO_API_KEY        = "/Configuration"

ABSOLUTE_PATH_TO_TEMPLATES = common_stuff.ABSOLUTE_PATH_TO_PROJECT_ORIGIN + RELATIVE_PATH_TO_TEMPLATES
ABSOLUTE_PATH_TO_API_KEY   = common_stuff.ABSOLUTE_PATH_TO_PROJECT_ORIGIN + RELATIVE_PATH_TO_API_KEY


'''
This function reads the API key from a configuration file stored locally.
This allows operating without keeping the key in any source code file.

param  - none

return - api key  
'''

def ReadApiKeyFromFile () :

    API_CONFIG_FILE_NAME = "Sendgrid_Config"

    wd_upon_call = os.getcwd()

    os.chdir(ABSOLUTE_PATH_TO_API_KEY)

    #TODO: this assumes that the api-key is the only thing in the file (and is saved as-is). we should probably create a smarter format, to allow adding further parameters.

    #open, read and close the file
    with open(API_CONFIG_FILE_NAME , 'r') as myfile :
        api_key = myfile.read().rstrip()


    #go back to original directory, to avoid side-effects
    os.chdir(wd_upon_call)

    return (api_key)

#main flow:

log_layer.InitLog ()

try :

    API_KEY = ReadApiKeyFromFile ()

except Exception as ex :

    log_layer.WriteToLog (ex , "Failed to reap API key!")
    log_layer.WriteToLog (ex , "error")
    exit ()


#ensure we always start at the correct working directory
os.chdir (ABSOLUTE_PATH_TO_TEMPLATES)

app = Flask (__name__)
api = Api (app)

parser = reqparse.RequestParser ()
parser.add_argument ('subject')
parser.add_argument ('template_name')
parser.add_argument ('origin_address')
parser.add_argument ('destination_address')
parser.add_argument ('substitutions_json')


class EmailSender (Resource) :

    #send formatted email
    def post (self) :

        log_layer.WriteToLog ("in SendEmail post routine!" , "info")

        try:

            args = parser.parse_args()
            email_subject = args['subject']
            template_name = args['template_name']
            origin_address = args['origin_address']
            destination_address = args['destination_address']
            substitutions_json = args['substitutions_json']

        except Exception as ex :

            log_layer.WriteToLog (ex , "error")

            return ("Error in parsing arguments", 400)

        else:

            log_layer.WriteToLog (email_subject , "info")
            log_layer.WriteToLog(template_name , "info")
            log_layer.WriteToLog (origin_address , "info")
            log_layer.WriteToLog (destination_address , "info")

            os.chdir (ABSOLUTE_PATH_TO_TEMPLATES)
            os.system ("pwd")

            try :

                substitutions = ast.literal_eval (substitutions_json)
                email_layer.FormatAndSendEmail (origin_address , destination_address , email_subject , template_name , substitutions)

            except Exception as ex :

                log_layer.WriteToLog (ex , "error")

                return ("Error formatting or sending the email", 400)
            else:

                return (200)

class EmailDetails (Resource) :

    #get list of filler, required to be substituted for a given template
    #def get (self , template_name) :
    def get(self) :

        log_layer.WriteToLog ("got GET request for fillers list" , "info")

        try:

            args = parser.parse_args ()
            template_name = args['template_name']
            log_layer.WriteToLog ("template_name %s" % (template_name) , "info")
            fillers_list = email_layer.GetFillersList (template_name)

        except Exception as ex:

            log_layer.WriteToLog(ex , "error")

            return ("Error in retrieving fillers list", 400)

        else:

            return (fillers_list , 200)

class EmailsList (Resource) :

    #get list of template names
    def get(self) :

        log_layer.WriteToLog ("got GET request for list of templates" , "info")

        try :

            templates_list = email_layer.GetTemplatesList ()

        except Exception as ex :

            log_layer.WriteToLog (ex , "error")

            return ("Error in retrieving templates list" , 400)
        else :
            return (templates_list , 200)


##
## Actually setup the Api resource routing here
##
api.add_resource (EmailSender , '/SendEmail')
api.add_resource (EmailDetails , '/EmailDetails')
api.add_resource (EmailsList , '/EmailsList')


if __name__ == '__main__' :

    app.run(debug=True)

#######################################################################################################################################################################################3